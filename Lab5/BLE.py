#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file BLE.py

This file cosists of an Bluetooth class file which has the main operation to
use pyb class UART to read, write, and wait for a character. This file is also
conected to pin A5 of the board which allows the control of LED power ON/OFF. 

Created on Sun Nov  8 20:59:03 2020

@author: adanmartinez
"""
from pyb import UART
import pyb

class BLEdriver:
    '''
    @brief      A group of methods that allow interface with users inputs via
                bluetooth application which main goal is to interface with user.
    @details    This class implements UART classes imported from pyb. It 
                facillitates interface with user.
    '''
        
    def __init__(self):
        '''
        @brief            Creates an Bluetooth Driver object.
        '''
        ## Setting up UART.
        self.uart = UART(3, 9600)
        
        ## Setting Up pyb Pin A5
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

    def check(self):
        '''
        @brief      This method check/waits whenever the user may enter any
                    character.
        '''
        self.uart.any()
        
    def read(self):
        '''
        @brief      This method reads the user input and returns the value as
                    a byte type. 
        '''
        self.uart.readline()
        
    def write(self):
        '''
        @brief      This method writes the desired value/string/data type. 
        '''
        self.uart.write()
        
    def PowerON(self):
        '''
        @brief      This method turns the LED ON in the Nucleo Board.
        '''
        self.pinA5.high()
        
    def PowerOFF(self):
        '''
        @brief      This method turns the LED OFF in the Nucleo Board.
        '''
        self.pinA5.low()
        