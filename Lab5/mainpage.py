## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @package mainpage
#  Brief doc for setting up main page.
#
#  @mainpage
#
#  @author Adan Martinez
#
#  @date Nov. 7, 2020
#
#  @section sec_intro Introduction
#  The purpose behind this project is to create a program that interacts with 
#  user via bluetooth to blink LED light between 1-10Hz.
#
#  @section Thunkable Application
#  The project also includes an iOS application that connects with the user via
#  the bluetooth that was added to the Board. The app facilates the interface between the
#  user and the board. Because of some technical reason, I was unable to download and use the 
#  program, I had to use the DSDTECH Bluetooth App which also allows the user to send and receive
#  data to or from the Board. The link below shows a quick sample of the program in Thunkable.
#       https://x.thunkable.com/projects/5fa9e49d392c5400139fb147/dd24d78d-df2f-440e-a6ad-924c886146b1/blocks
#
#  @section Respository
#  @ref BitBucket Respository:
#       https://bitbucket.org/amart426/me305_labs/src/master/Lab5/ and
#       https://amart426@bitbucket.org/amart426/amart426.bitbucket.io.git 
# 
#  @image html LAB5.png