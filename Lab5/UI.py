#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file UI.py

This file cosists of a Finate State Machine User Interface class that has 
the main operation of blinking the LED light of a Nucleo-L476RG.

This file is meant to stricly run in micropython since it uses pyb. The code 
implemented main goal is to receive a desired frequency value from a Bluetooth
paired mobile phone application. This FSM will accept frequency values from
1 -10. Entering a 1 will mean the LED will blink at a 1Hz frequency.
The creates a counter variable that counts every time the task is run once.
This counter allows control of when to transition between states and when to
turn on and off the LED.

Note:  For some reason the BLEdriver was not able to perform as its supposed
       to. This forced this file to incorporated pyb UART classes directly.
       Which in turn made the FSM more busy.

Created on Fri Nov 10 01:29:21 2020

@author: Adan Martinez
"""
import utime
from pyb import UART
#from BLE import BLEdriver
import pyb

class user_interface:
    '''
    @brief      A finite state machine which main goal is to interface with user.
    @details    This class implements a finite state machine to interface with user 
                designed specifically to run in Micropython.
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT      = 0    
    ## Constant defining State 1
    S1_CHECKING  = 1    
    ## Constant defining State 2
    S2_BLINKING  = 2    
    def __init__(self, interval):
        '''
        @brief            Creates a user_interface object.
        @param interval   Representing the elapsed time count(microsecond) 
                          between user state interface. Please leave intact.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*(1e3))
        ## Counter that describes the number of times the task has run
        self.counter = 0
        
        ## The timestamp for the first iteration 
        self.start_time = utime.ticks_us()
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        #self.BLE = BLEdriver object from module
        #self.BLE = BLEdriver
        
        ## Sets up UART baud rate and UART(3)
        self.uart = UART(3, 9600)
        self.uart.init(9600, bits=8, parity=None, stop=1)
        
        ## Setups the pin for LED control
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        ## Sets the initial frequency variable equal to zero.
        self.freq = 0
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Updating current timestamp
        self.curr_time = utime.ticks_us()
        
        ## State 0
        if(self.state == self.S0_INIT):
            print('Initiallizing')
            self.transitionTo(self.S1_CHECKING) #Transition
            
        ## State 1
        elif(self.state == self.S1_CHECKING):
            
            ##First, checks for any characters entered and validates the input.
            if self.uart.any() != 0:
                print('Validating')
                ## reads and returns the input as a byte type and saves it as val.
                val = self.uart.readline()
                ## Converts val from a byte type to an integer.
                self.freq = int.from_bytes(val, "little")
                
                ## Checks if freq is between 1-9
                if self.freq >= 1 and self.freq <= 9: #1-9
                    ## Zeros the counter
                    self.counter = 0
                    ## Creates a variable that represents the period.
                    self.period = self.interval/self.freq
                    ## Transition to State 1
                    self.transitionTo(self.S2_BLINKING)
                    
                ## Since b'\x10' when converted to an integer equal 16 for unknown reason.
                #  to compesate for this, the frequency is then set to equal ten.
                elif self.freq == 16: # Freq = 10 or b'\x10'
                    self.freq = 10
                    ## Calcs the period.
                    self.period = self.interval/self.freq
                    ## Resets the counter.
                    self.counter = 0
                    ## Transition to State 2.
                    self.transitionTo(self.S2_BLINKING)
                else:
                    ## Handling Non-valid Integers
                    print('Please enter a valid integer from 1-10.')
                    self.transitionTo(self.S2_BLINKING)
                    
            else:
                ## Handling non inputs
                print('Please enter an interger from 1-10.')
                self.transitionTo(self.S1_CHECKING)
        
        
        elif(self.state == self.S2_BLINKING):
            ## Runs State 2
            
            ## Sets condition to ensure that the LED is only ON for half of the period. 
            #  It uses the counter technique, it will onyl transition once the value of 
            #  the counter exceeds the magnitude of the period/2.
            if self.counter > 0 and self.counter <= (self.period/2):
                print(str(self.counter) + '  Power ON' + ' at Frequency: ' +str(self.freq))
                ## Turns ON the LED
                self.pinA5.high()
                
            ## Sets condition to ensure that the LED is only OFF for half of the period. 
            #  It uses the counter technique, it will onyl transition once the value of 
            #  the counter exceeds the magnitude of the period/2.
            elif self.counter >  (self.period/2) and self.counter < self.period:
                ## Turns OFF the LED
                self.pinA5.low()
                print(str(self.counter) + '  Power OFF' + ' at Frequency: ' +str(self.freq))
            
            ## Checks if any charecter is enter while in state 2, and if so, it transition to
            #  state 1 to validate the input.
            elif self.uart.any() != 0:
                self.transitionTo(self.S1_CHECKING)
            
            
            else:
                self.counter = 1
                self.transitionTo(self.S2_BLINKING)
        ## Continously adds a counter each time state 2 is run.
        self.counter += 1
        # Specifying the next time the task will run
        self.next_time += utime.ticks_add(self.next_time, self.interval)
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        