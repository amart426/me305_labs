var searchData=
[
  ['parity_19',['parity',['../namespace_b_ttest2.html#a2c09321524ca5423624bb3931d4dac43',1,'BTtest2']]],
  ['period_20',['period',['../class_u_i_1_1user__interface.html#ac49909e4d4b500caa75e16013cd75095',1,'UI::user_interface']]],
  ['pina5_21',['pinA5',['../class_b_l_e_1_1_b_l_edriver.html#a155a0625f328b7a491e024210b3ea0d6',1,'BLE.BLEdriver.pinA5()'],['../class_u_i_1_1user__interface.html#a9d6d17e5f8ccb147f213eddd01c266ea',1,'UI.user_interface.pinA5()'],['../namespace_b_ttest2.html#a64e69bbea7d3a95c9c4a84c5a263555d',1,'BTtest2.pinA5()']]],
  ['poweroff_22',['PowerOFF',['../class_b_l_e_1_1_b_l_edriver.html#a2213c07fffb23e4a1d4c4d4b4f839713',1,'BLE::BLEdriver']]],
  ['poweron_23',['PowerON',['../class_b_l_e_1_1_b_l_edriver.html#a7243281722b3a58d3767a000ae2a3ee2',1,'BLE::BLEdriver']]]
];
