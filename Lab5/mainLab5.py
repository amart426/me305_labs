#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file mainLab5.py

This file consist as the main file. This file can be run directly in the Nucleo Board.
Again, for complications with the Bluetooth driver I was not able to run it sucessfully with
the User Interface FSM.

       
Created on Thu Nov 12 03:43:02 2020

@author: adanmartinez
"""

#from BLE import BLEdriver
from UI import user_interface

## No need to change the interval. The interval is already set to run at 1 mili second.
task1 = user_interface(1)
while True:
    task1.run()