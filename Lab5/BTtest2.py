#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  8 19:12:54 2020

@author: adanmartinez
"""
from pyb import UART
import pyb

## Setting up UART
myuart = UART(3, 9600)
myuart.init(9600, bits=8, parity=None, stop=1)

## Setting Up pyb Pin A5
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

while True:
    if myuart.any() != 0:
        val = myuart.readline()
        print(val)
        val2 = int.from_bytes(val, "little")
        print(val2)
        val3 = val.decode()
        print(val3)
        if val == b'\x00':
            print(val,' turns it OFF')
            pinA5.low() 
        elif val == b'\x01':
            print(val,' turns it ON') 
            pinA5.high()
            