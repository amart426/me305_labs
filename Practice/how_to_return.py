#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 10 16:14:58 2020

@author: adanmartinez
"""

def func_return():
    a = 10
    return a

def no_return():
    a = 5
    return a
print(func_return())
print(no_return())