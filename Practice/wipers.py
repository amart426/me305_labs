#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 11 20:50:33 2020

@author: adanmartinez
"""

class TaskWindshield:
    S0_INIT = 0
    def __init__(self):
        self.state = self.S0_INIT
    def run(self):
        if(self.state == self.S0_INIT):
            pass
    def transition(self, newstate):
        self.state = newstate
        