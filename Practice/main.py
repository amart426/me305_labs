#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 18:20:45 2020

@author: adanmartinez
"""
from pyb import UART

myuart = UART(2)
while True:
    if myuart.any() != 0:
        val = myuart.readline()
        myuart.write('You sent an ASCII ' + str(val) + ' to the Nucleo')
