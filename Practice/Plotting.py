#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 18:21:58 2020

@author: adanmartinez
"""

import matplotlib.pyplot as plt

# plt.plot([1, 2, 3, 4])
# plt.ylabel('Numbers')
# plt.show()

## Create a plot.
plt.plot([1,2,3,4,5,6], 'r')
plt.xlabel('Time [s]')
plt.ylabel('Speed. [rad/s]')
plt.grid(True)
plt.show()

# ## Save data to a .csv file.
# np.savetxt('Lab6.csv', shares.speed, delimiters=',')

