#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 10:19:51 2020

@author: adanmartinez
"""
import matplotlib.pyplot as plt
import numpy as np
import time


def ddx_p(x):
    ddx_p = -k*x - alpha*x**3
    return ddx_p

def dx_f(x):
    dx_f = dx_p + 0.5*(ddx_p + ddx_f)*dT
    return dx_f
def x_f(x):
    x_f = x_p
    return x_f
    
## Constants
k = 2
alpha = 5

## Initial Conditions
# xdot(0)
dx[0] = 0
# x(0)
x[0] = x0
# Delta T
dT = 0.01

## Leapfrog method

for i in range(1, dT)

    x[k+1] = x + dx*dT + 0.5*ddx*dT^2

    