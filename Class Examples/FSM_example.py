'''
@file FSM_example.py

This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary windshield
wiper.

The user has a button to turn on or off the windshield wipers.

There is also a limit switch at either end of travel for the wipers.
'''

from random import choice

class TaskWindshield:
    '''
    @brief      A finite state machine to control windshield wipers.
    @details    This class implements a finite state machine to control the
                operation of windshield wipers.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT = 0
    ## Constant defining State 1
    S1_SOTPPED_AT_LEFT = 1
    ## Constant defining State 2
    S2_STOPPED_AT_RIGHT = 2
    ## Constant defining State 3
    S3_MOVING_LEFT = 3
    ## Constant defining State 4
    S4_MOVING_RIGHT = 4
    def __init__(self):
        '''
        @brief      Creates a TaskWindshield object.
        '''
        
        ## The state to run on the next iteration of the task.
        if(self.state = self.S0_INIT)
        
        elif(self.state = self.S1_SOTPPED_AT_LEFT)
        elif(self.state = self.S2_SOTPPED_AT_RIGHT)
        elif(self.state = self.S3_MOVING_LEFT)
        elif(self.state = self.S4_MOVING_RIGHT)

    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        if(self.state = self.S0_INIT);
        elif(self.state = self.S3_MOVING_LEFT)
        elif(self.state = self.S2_SOTPPED_AT_RIGHT)
        elif(self.state = self.S3_SOTPPED_LEFT)
        elif(self.state = self.S4_STOPPED_RIGHT)
        if(self.state == self.S0_INIT):
            # Run State 0 Code
            pass
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        


class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])



class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Forward(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor moving CCW')
    
    def Reverse(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor moving CW')
    
    def Stop(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor Stopped')

