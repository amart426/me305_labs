## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @package mainpage
#  Brief doc for setting up main page.
#
#  @mainpage
#
#  @author Adan Martinez
#
#  @date Jan. 4, 2020
#
#  @section me_305 ME 305 Documentation
#  https://bitbucket.org/amart426/amart426.bitbucket.io/src/master/me_305/
#
#  @section me_405 ME 405 Documentation
#  https://bitbucket.org/amart426/amart426.bitbucket.io/src/master/me_405/
#