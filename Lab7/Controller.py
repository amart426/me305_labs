#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file Controller.py

This file cosists of a Finate State Machine Controller class that has the main
operation to bring together the firmware and physical hardware. 
The way of implementing this task is by creating creating object from MotorDriver,
EncoderDriver, ClosedLoop and spin the motor at some duty cycle and recording the
actual speed.

Created on Fri Nov 10 01:29:21 2020

@author: Adan Martinez
"""
import utime
from UI_back import Task_User
from Motor import MotorDriver
from encoder import EncoderDriver
from CL import ClosedLoop
import shares

class Controller:
    '''
    @brief      A FSM for spinning and obtaining spining values.
    @details    This class implements a finite state machine to interface with user 
                designed specifically to run in Micropython.
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT        = 0    
    ## Constant defining State 1
    S1_CHECK_KP    = 1    
    ## Constant defining State 2
    S2_SPIN_MOTOR  = 2    
    ## Constant defining State 3
    S3_STOP_MOTOR  = 3
    
    def __init__(self, interval):
        '''
        @brief            Creates a user_interface object.
        @param interval   Representing the elapsed time count(microsecond) 
                          between user state interface. Please leave intact.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*(1e6))
        ## Counter that describes the number of times the task has run
        self.counter = 0
        
        ## The timestamp for the first iteration 
        self.start_time = utime.ticks_us()
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        # Creates a object from encoderdriver module
        self.BACK = Task_User()
        self.ENC = EncoderDriver()
        self.motor = MotorDriver()
        
        self.J = 0
        self.K = 100
        
        #self.CL = ClosedLoop(self.BACK.get_Kp(), self.BACK.get_ome_ref, self.ome_meas)
        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Updating current timestamp
        self.curr_time = utime.ticks_us()
        
        ## State 0
        self.Kp = self.BACK.get_Kp()
        if(self.state == self.S0_INIT):
            print('Initiallizing')
            self.transitionTo(self.S1_CHECK_KP) #Transition
            
        ## State 1
        elif(self.state == self.S1_CHECK_KP):
            
            ##First, checks for any characters entered and validates the input.
            if self.Kp != None:
                if self.Kp > 0 and self.Kp <= 100:
                    print('Validating')
                    self.transitionTo(self.S2_SPIN_MOTOR)                     
            else:
                ## Handling non inputs
                print('Please enter a valid Kp value.')
                self.transitionTo(self.S1_CHECK_KP)
        
        elif(self.state == self.S2_SPIN_MOTOR):
            ## Runs State 2
            if self.counter >= 0 and self.counnter < 1: 
                self.motor.enable()
                self.ENC.set_position()
                self.motor.set_duty(50)
                self.ENC.update()
                self.counter = 1
            elif self.counter > 1 and self.counter <= self.K:   
                self.ome_meas = (self.ENC.get_angle()/self.interval)*0.0174532925
                self.CL = ClosedLoop(self.Kp, self.BACK.get_ome_ref(), self.ome_meas)
                self.CL.update()
                self.motor.set_duty(self.CL.update())
                self.ENC.update()
                self.ome_act = (self.ENC.get_angle()/self.interval)*0.0174532925
                self.end_time = utime.ticks_diff(self.curr_time, self.interval/2)
                self.J = (1/self.K)*((shares.ome_ref[':'] - self.ome_act)^2 + (shares.angel[':'] - self.ENC.get_angle)^2)
            elif self.counter > self.K:
                self.transitionTo(self.S3_STOP_MOTOR)
            
            else:
                self.transitionTo(self.S2_SPIN_MOTOR)
            
        elif(self.state == self.S3_STOP_MOTOR):
            self.ENC.update()
            self.motor.set_duty(0)
            self.ENC.update()
            self.motor.disable()
            
        # Specifying the next time the task will run
        self.next_time += utime.ticks_add(self.next_time, self.interval)
        self.counter += 1
        
        self.ome_act = (self.ENC.get_angle()/self.interval)*0.0174532925 # rad/sec
 
        shares.Speed.append(self.ome_act)
        shares.time.append(self.end_time)
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
    
    
    
