#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file     shares.py

@brief    This file containts arrays that are use interchangeably across
          the tasks.
          
Created on Thu Nov  1 01:27:03 2020

@author: adanmartinez
"""
## Importing data
Kp      = 0
interval = None
ome_ref = []
time    = []
angle   = []

## Export Data
Time    = [0]
Speed   = [0]

