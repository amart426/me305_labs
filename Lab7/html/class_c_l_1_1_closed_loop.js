var class_c_l_1_1_closed_loop =
[
    [ "__init__", "class_c_l_1_1_closed_loop.html#a46e95dfe28365e257d0087d2ae2dd728", null ],
    [ "get_Kp", "class_c_l_1_1_closed_loop.html#a1ab2850ae8c489cabd51ea5372ac32e1", null ],
    [ "set_Kp", "class_c_l_1_1_closed_loop.html#a757d413e1494195d6c1ae66d863a2a08", null ],
    [ "update", "class_c_l_1_1_closed_loop.html#a60d905003f0a9ffce25cc3b2be4f0ad9", null ],
    [ "Kp", "class_c_l_1_1_closed_loop.html#a333e5de135c87770ede0ce606784bfe2", null ],
    [ "L", "class_c_l_1_1_closed_loop.html#a6de2aead7f4d91ac9498dd563c646fad", null ],
    [ "ome_meas", "class_c_l_1_1_closed_loop.html#a7a8cf422cd77b3b7518fd333831a36d8", null ],
    [ "ome_ref", "class_c_l_1_1_closed_loop.html#a2dff7f76881ead4326ac3e7902994e83", null ]
];