#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file UI_front.py

The purpose of this code is to only run in Python. It uses serial port to send
and receive the motors speed and plots the first order response. 

Created on Mon Nov 23 18:55:05 2020

@author: adanmartinez
"""
import serial
import matplotlib.pyplot as plt
import numpy as np

# Connects to port. Change port name if different. THe baudrate must match
#   that of putty/iTerm.
#ser = serial.Serial(port='/dev/tty.usbmodem2051339257522',baudrate=115200,timeout=(1))
ser = serial.Serial(port='/dev/tty.usbmodem14503',baudrate=115200,timeout=(1))




   
Kp = input('Enter value for Kp: ')
ser.write(Kp.encode('ascii'))


## Creating list to store retrive data from Nucleo
data  = [] 
speed = []
time  = []


data = ser.readline().decode()
#sort = data.strip().split(',')

for n in range(1):
    #sort = data.split(',')
    speed.append(data[0])
    time.append(data[1])

    
ser.close()   

## Create a plot.
plt.plot(time, speed, 'r')
plt.xlabel('Time [s]')
plt.ylabel('Speed. [rad/s]')
plt.grid(True)
plt.show()

## Save data to a .csv file.
np.savetxt('Lab7.csv', speed, time, delimiters=",")


