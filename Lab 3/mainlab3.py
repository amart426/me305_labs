#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
@file mainlab3.py

This code serves as the main project.

This code is where both the FSM_encoder.py and FSM_UI.py are run cooperatively

@author: Adan Martinez Cruz

@date October 14, 2020
'''
import pyb
from encoder import EncoderDriver
from FSM_ENC import Task_ENC
from FSM_UI import user_interface

## Assigns Nucleo Board Pins to use. Change if needed. 
A6 = pyb.Pin.cpu.A6
A7 = pyb.Pin.cpu.A7

## Calls EncoderDriver class and calls it enc.
enc = EncoderDriver(A6, A7)

## Creating a task object using the interval. The user can change the time 
#  interval here. The user can specify a different interval value. 
Task1 = Task_ENC(0.001, enc)
Task2 = user_interface(0.001, enc)

while True:
    #Run task 1
    Task1.update()        
    
    #Run task 2
    Task2.update()

