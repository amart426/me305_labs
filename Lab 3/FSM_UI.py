#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file FSM_UI.py

This file cosists of an Encoder class file which has the main operation to use 
the timer counter to read from an encoder conected to arbitrary pins. 

This file is meant to stricly run in micropython since it uses pyb. The 
code implemented main goal is to read from timer counter to control and obtain an 
encoders position when connected through a Nucleo Board.

Created on Fri Oct 23 01:29:21 2020

@author: Adan Martinez
"""
import utime
from pyb import UART
from encoder import EncoderDriver

class user_interface:
    '''
    @brief      A finite state machine which main goal is to interface with user.
    @details    This class implements a finite state machine to interface with user 
                designed specifically to run in Micropython.
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT    = 0    
    
    ## Constant defining State 1
    S1_UPDATE  = 1    
    
    ## Constant defining State 2
    S2_UPDATE  = 2

    
    def __init__(self, interval):
        '''
        @brief            Creates a user_interface object.
        @param interval   Representing the elapsed time count(microsecond) 
                          between user state interface.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT

        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*(1e6))
        
        ## The timestamp for the first iteration 
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        self.uart = UART(2, 9600)
        self.uart.init(9600, bits=16, parity=None, stop=1)
        
        
        ## Calls the update() method from EncoderDriver class.
        self.encoder = EncoderDriver.update()
        
        self.setPos = EncoderDriver.set_position()
        
        self.getPos = EncoderDriver.get_position()
        
        self.getDelt = EncoderDriver.get_delta()
        
        self.user_input = self.uart.any()
        
        
    def update(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us() # Updating current timestamp     

        if(self.state == self.S0_INIT):
            print('initiallizing')
            self.transitionTo(self.S1_UPDATE)
            
        elif(self.state == self.S1_UPDATE):
            self.uart.any()
            if(self.user_input == 1111010): # z
                print('Encoder position is: ' + str(self.setPos))
                
            elif(self.user_input == 1110000): # p
                print('Encoder position is: ' + str(self.getPos))
                
            elif(self.user_input == 1100100): # d
                print('Delta is ' + str(self.getDelt))
            else:
                self.uart.readchar()            
                self.transitionTo(self.S2_UPDATE)
            self.runs += 1
            # Specifying the next time the task will run
            self.next_time += utime.ticks_add(self.next_time, self.interval)            
                
        elif(self.state == self.S2_UPDATE):
            if(self.user_input == 1111010): # z
                print('Encoder position is: ' + str(self.setPos))
                
            elif(self.user_input == 1110000): # p
                print('Encoder position is: ' + str(self.getPos))
                
            elif(self.user_input == 1100100): # d
                print('Delta is ' + str(self.getDelt))
            else:
                self.uart.readchar()            
                self.transitionTo(self.S1_UPDATE)
            
            self.runs += 1
            # Specifying the next time the task will run
            self.next_time += utime.ticks_add(self.next_time, self.interval)
            
    def transitionTo(self, newState):
        
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
    
    