#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  7 17:18:06 2020

@author: Adan Martinez

@file main_sec_01.py
"""

from ele_test import Button, MotorDriver, ElevatorCar

# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructor

button_1 = Button('PB6')
button_2 = Button('pb4')
second = Button('PB7')
first = Button('PB8')
Motor = MotorDriver()

# Creating a task object using the button and motor objects above
task1 = ElevatorCar(0, button_1, button_2, second, first, Motor)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
#    task2.run()
#    task3.run()