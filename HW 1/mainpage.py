## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @package mainpage1
#  Brief doc for setting up main page.
#
#  @mainpage
#
#  @author Adan Martinez
#
#  @date Oct 7, 2020
#
#  @section sec_intro Introduction
#  The purpose behind this project is to simulate the control of an elevator 
#  car using Finite State Machine method.
#  @image html FSM_Car_Elevator.png

