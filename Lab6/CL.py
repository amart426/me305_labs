#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file CL.py

This file cosists of an ClosedLoop class file which has the main operation to
calculate the actual actuation from the motors using the reference values 
and measure values from encoder. This code also use a proportional gain Kp
receive from user. This file also incorporates two methods that allows user
to get and set the value for the proportional gain. The actuaction value is
return when the method update is used by user. 

Created on Mon Nov 23 17:21:34 2020

@author: adanmartinez
"""


class ClosedLoop:
    
    def __init__(self, Kp, ome_ref, ome_meas):
        '''               
        @brief            Creates an Encoder object.
        @details          A constructor init () which does the setup, given appropriate 
                          parameters such as the initial proportional gain for the system 
                          and perhaps saturation limits on the PWM level. Would it make 
                          sense to request more than 100% effort from the motor?
        @param Kp         An object from class Kp representing the proportional
                          gain.
        @param ome_ref    An object from class ome ref representing the reference
                          speed.
        @param ome_meas   An object from encoder class representing the measured
                          speed value.
        '''

        ## Creates an object for the speed reference value.
        self.ome_ref   =  ome_ref
        ## Creates an object for the measured speed value.
        self.ome_meas  =  ome_meas
        ## Sets the initial value for the actuation value.
        self.L         =  0
        ## Creates an object for the proportional gain.
        self.Kp        =  Kp
        
    def update(self):
        '''
        @detail  Computes and returns the actuation value based on the reference 
                 and measured values
        '''
        ## Calculating L
        self.L = int((self.ome_ref - self.ome_meas)*self.set_Kp())
        if self.L > 0 and self.L <= 100:
            return self.L
        
        else:
            print('Error... Please Enter new Kp gain.')
            pass
        return self.L

    def get_Kp(self):
        '''
        @brief      This method returns the proportional gain. 
        '''
        return self.Kp

    def set_Kp(self, newValue):
        '''
        @brief      This method usets the value for the proportional gain Kp. 
        '''
        self.Kp = newValue
        
