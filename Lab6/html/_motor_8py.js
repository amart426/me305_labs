var _motor_8py =
[
    [ "MotorDriver", "class_motor_1_1_motor_driver.html", "class_motor_1_1_motor_driver" ],
    [ "ch1", "_motor_8py.html#a6f04475e46996954e8aa4aee2167cb6d", null ],
    [ "ch2", "_motor_8py.html#a4c1ecb29c348c5c80b48cffa5c1eb280", null ],
    [ "ch3", "_motor_8py.html#a868a0b0553863cee5482cc1f9f45ee04", null ],
    [ "ch4", "_motor_8py.html#a756d36ac03a7750202cb8cc8e64326c5", null ],
    [ "moe1", "_motor_8py.html#a7f7bacd1831683af33c7a28ea4880f63", null ],
    [ "moe2", "_motor_8py.html#ad5609c3da890fb0a9bd0c0cb7e0c60f2", null ],
    [ "pin_IN1", "_motor_8py.html#aedf33ecfa88f9b52ef256e9712af1284", null ],
    [ "pin_IN2", "_motor_8py.html#a5e2cfbb46c87b18668f3cf80603c811b", null ],
    [ "pin_IN3", "_motor_8py.html#a21fd14ddefedaac3cbb2c968e306b94c", null ],
    [ "pin_IN4", "_motor_8py.html#a417a807ea7c0f67847fd91f147a96666", null ],
    [ "pin_nSLEEP", "_motor_8py.html#a9ac69510c84de8e30e26ddf30e385ab8", null ],
    [ "timer", "_motor_8py.html#aa4f7849df2375923ed5f7c9cd3e21aa6", null ]
];