var searchData=
[
  ['ch1_142',['ch1',['../namespacemain.html#aa0d92a8184196ffe1022c05a518e6df7',1,'main.ch1()'],['../namespace_motor.html#a6f04475e46996954e8aa4aee2167cb6d',1,'Motor.ch1()']]],
  ['ch2_143',['ch2',['../namespacemain.html#a1b83156e1532940ea3b40bfd3b14f981',1,'main.ch2()'],['../namespace_motor.html#a4c1ecb29c348c5c80b48cffa5c1eb280',1,'Motor.ch2()']]],
  ['ch3_144',['ch3',['../namespacemain.html#a68ebb90386fa3fb81ef8b2a23c1a0d4a',1,'main.ch3()'],['../namespace_motor.html#a868a0b0553863cee5482cc1f9f45ee04',1,'Motor.ch3()']]],
  ['ch4_145',['ch4',['../namespacemain.html#a02f67e56867aae4c6b7c446e2dcffd76',1,'main.ch4()'],['../namespace_motor.html#a756d36ac03a7750202cb8cc8e64326c5',1,'Motor.ch4()']]],
  ['channela_146',['channelA',['../class_motor_1_1_motor_driver.html#ae06f0ed23bcad40f6ceea593b554df1f',1,'Motor::MotorDriver']]],
  ['channelb_147',['channelB',['../class_motor_1_1_motor_driver.html#abce6b896fd591528bd641518563fca2e',1,'Motor::MotorDriver']]],
  ['cl_148',['CL',['../class_controller_1_1_controller.html#a8746925b605939ce1d48fc7fd6b77a43',1,'Controller::Controller']]],
  ['counter_149',['counter',['../class_controller_1_1_controller.html#a46cfa8ea94ec85fc1e4311cf18d9e7d0',1,'Controller.Controller.counter()'],['../classencoder_1_1_encoder_driver.html#adc4878891e49ea8fab960c4fd3f8692f',1,'encoder.EncoderDriver.counter()'],['../class_u_i__back_1_1_task___user.html#a7250eea50271a7e136a671b2c45960f4',1,'UI_back.Task_User.counter()']]],
  ['curr_5ftime_150',['curr_time',['../class_controller_1_1_controller.html#ab659884baa58968621c6245d66eaf62d',1,'Controller.Controller.curr_time()'],['../classencoder_1_1_encoder_driver.html#a2e44d3d688d044a92c8a16aa0f98d55a',1,'encoder.EncoderDriver.curr_time()'],['../class_u_i__back_1_1_task___user.html#aa38b80ef8d15e7ef4bd5381bc7d6e554',1,'UI_back.Task_User.curr_time()']]]
];
