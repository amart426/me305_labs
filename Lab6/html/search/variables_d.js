var searchData=
[
  ['s0_5finit_189',['S0_INIT',['../class_controller_1_1_controller.html#a8d4383947883df4d22acd2fef0184b7a',1,'Controller.Controller.S0_INIT()'],['../class_u_i__back_1_1_task___user.html#a565152a9733be5b9b67081f621a49211',1,'UI_back.Task_User.S0_INIT()']]],
  ['s1_5fcheck_5fkp_190',['S1_CHECK_KP',['../class_controller_1_1_controller.html#acbfa3fb911032ca92dedc3feadcd3e18',1,'Controller::Controller']]],
  ['s1_5fchecking_191',['S1_CHECKING',['../class_u_i__back_1_1_task___user.html#a4058284a8b96a1ecb0a853b1cd79ccc3',1,'UI_back::Task_User']]],
  ['s2_5fsend_5fdata_192',['S2_SEND_DATA',['../class_u_i__back_1_1_task___user.html#a9e955c81c489ad5da013fd8cdac5d2d1',1,'UI_back::Task_User']]],
  ['s2_5fspin_5fmotor_193',['S2_SPIN_MOTOR',['../class_controller_1_1_controller.html#a80dce4ecfe8f187ec32e0374130b6553',1,'Controller::Controller']]],
  ['s3_5fstop_5fmotor_194',['S3_STOP_MOTOR',['../class_controller_1_1_controller.html#a37e02f7cfd9d4e5559dbd481232cb5c2',1,'Controller::Controller']]],
  ['ser_195',['ser',['../namespace_u_i__front.html#a26d65cb7ea7d47e295007598eedfb6ba',1,'UI_front']]],
  ['sort_196',['sort',['../namespace_u_i__front.html#a89ae32e0d4950c9a37c66c62236b345c',1,'UI_front']]],
  ['speed_197',['Speed',['../namespaceshares.html#adebca76ba9bbdba3c2a68dee1b122ddc',1,'shares.Speed()'],['../namespace_u_i__front.html#a4011c4e3ceda99aba1bb0bee64282e6b',1,'UI_front.speed()']]],
  ['start_5ftime_198',['start_time',['../class_controller_1_1_controller.html#a17544c1b76e8509ac2743299c19b516c',1,'Controller.Controller.start_time()'],['../classencoder_1_1_encoder_driver.html#a519e5f33b6f0fbdcd76690e2ebeeaeab',1,'encoder.EncoderDriver.start_time()'],['../class_u_i__back_1_1_task___user.html#a17774a893a39dbd640139e9bbbda42f0',1,'UI_back.Task_User.start_time()']]],
  ['state_199',['state',['../class_controller_1_1_controller.html#a024dce78fb3e60f5cfbd2ab60f14dbd3',1,'Controller.Controller.state()'],['../class_u_i__back_1_1_task___user.html#a28a553f95ba6a30be16292e747bfa72e',1,'UI_back.Task_User.state()']]]
];
