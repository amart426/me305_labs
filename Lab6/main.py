#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file main.py

This file cosists of the main file that only run in Nucleo Board. The file consist
of various objects from other classes. This file also serves a port for sending 
Kp value to our ClosedLoop driver and the time interval for running our controller. 

Created on Mon Nov 23 18:55:46 2020

@author: adanmartinez
"""
import pyb    
from Motor import MotorDriver

from Controller import Controller

from encoder import EncoderDriver
from UI_back import Task_User
from pyb import UART

## Setting Up Motor 1 & 2
# Setting up Pin locations
pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
pin_IN1     = pyb.Pin(pyb.Pin.cpu.B4) 
pin_IN2     = pyb.Pin(pyb.Pin.cpu.B5)
pin_IN3     = pyb.Pin(pyb.Pin.cpu.B0)
pin_IN4     = pyb.Pin(pyb.Pin.cpu.B1)
# Setting up timer.
timer = pyb.Timer(3, freq=20000)
# Setting up channels for motor
ch1 = 1
ch2 = 2
ch3 = 3
ch4 = 4
# Creatinng Motor Driver objects
MOT1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, ch1, ch2, timer)
MOT2 = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, ch3, ch4, timer)

## Creating Encoder Objects
ENC1 = EncoderDriver( 4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7)
ENC2 = EncoderDriver( 8, pyb.Pin.cpu.C6, pyb.Pin.cpu.C7)

## Setting time interval.
interval = 5

## Creating task from FSM
task1 = Task_User()
task2 = Controller(interval)

## Setting UART timer
myuart = UART(2)

while True:
    task1.run()
    task2.run()

