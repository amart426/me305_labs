## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @package mainpage
#  Brief doc for setting up main page.
#
#  @mainpage
#
#  @author Adan Martinez
#
#  @date October 14, 2020
#
#  @section sec_intro Introduction
#  The purpose behind this project is to create a program that runs both
#  a virtual LED simulation and a RELP of a PWD using a Nucleo Board.
#
#  @section Respository
#  @ref BitBucket Respository:
#       https://bitbucket.org/amart426/me305_labs/src/master/Lab%202/ and
#       https://amart426@bitbucket.org/amart426/amart426.bitbucket.io.git
# 
#  @image html FSMBlinkingLED.png