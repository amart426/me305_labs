#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import utime
import pyb

class BlinkLED:
    S0_INIT         = 0    
    S1_LED_ON_25    = 1    
    S2_LED_ON_50    = 2    
    S3_LED_ON_75    = 3 
    S4_LED_ON_100   = 4
    
    def __init__(self, interval):
        self.state = self.S0_INIT
        self.runs = 0
        self.interval = int(interval*(1e6))
        self.start_time = utime.ticks_us()                     
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
    def run(self):
        self.curr_time = utime.ticks_us()
    
        #if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
        
        if(self.state == self.S0_INIT):
            pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
            tim2 = pyb.Timer(2, freq = 20000)
            t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
            t2ch1.pulse_width_percent(0)
            print(str(self.runs) + ': LED 0')
            self.transitionTo(self.S2_LED_ON_50)
            
        elif(self.state == self.S1_LED_ON_25):
            pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
            tim2 = pyb.Timer(2, freq = 20000)
            t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
            t2ch1.pulse_width_percent(25)
            print(str(self.runs) + ': LED 25')
            self.transitionTo(self.S2_LED_ON_50)
            utime.sleep_us(200000)
        elif(self.state == self.S2_LED_ON_50):
            pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
            tim2 = pyb.Timer(2, freq = 20000)
            t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
            t2ch1.pulse_width_percent(50)
            print(str(self.runs) + ': LED 50')
            self.transitionTo(self.S3_LED_ON_75)
            utime.sleep_us(200000)
        elif(self.state == self.S3_LED_ON_75):
            pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
            tim2 = pyb.Timer(2, freq = 20000)
            t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
            t2ch1.pulse_width_percent(75)
            print(str(self.runs) + ': LED 75')
            self.transitionTo(self.S4_LED_ON_100)
            utime.sleep_us(200000)
        elif(self.state == self.S4_LED_ON_100):
            pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
            tim2 = pyb.Timer(2, freq = 20000)
            t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
            t2ch1.pulse_width_percent(100)
            utime.sleep_us(200000)
            print(str(self.runs) + ': LED 100')
            t2ch1.pulse_width_percent(0)
            self.transitionTo(self.S1_LED_ON_25)
            utime.sleep_us(40000)
        else:
            pass
        
        self.runs += 1
        self.next_time += utime.ticks_add(self.next_time, self.interval)
    def transitionTo(self, newState):
        self.state = newState
   
task2 = BlinkLED(0.0001)
for N in range(10000):
    task2.run()
