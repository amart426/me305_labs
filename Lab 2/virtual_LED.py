#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
@file virtual_LED.py

This file consist of a finite state machine that virtually simulates blinks 
of an LED.

This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control a virtual LED for 
Nucleo Board. This will be done by printing on the users screen when the LED
is ON and OFF state at a fixed interval.

The there are three main states in the FSM. The first state is the 
initialization, second state is when the LED is ON, and the LED is OFF in the
third state. The transition from one state to another consits of a fixed
interval commanded by the user.
'''
## UNCOMMENT for MicroPython
#import utime
#import pyb

## For running in Python only
import time

class VirtualLED:
    '''
    @brief      A finite state machine for virtually simulate controling LED.
    @details    This class implements a finite state machine to virtually 
                simulate the control operation of LED.
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT         = 0    
    
    ## Constant defining State 1
    S1_LED_ON       = 1    
    
    ## Constant defining State 2
    S2_LED_OFF      = 2
    
    ## Constant defining State 3
    S3_DO_NOTHING   = 3
    
    
    
    def __init__(self, interval):
        '''
        @brief              Creates a VirtualLED object.
        
        @param interval     A variable that allows the user to control the 
                            time interval between states
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The amount of time in seconds between runs of the task
        self.interval = interval
                                            
        ## UNCOMMENT '~' for hardware interaction using MicroPython
        ## The timestamp for the first iteration .
        # The microsencond timestamp for the task start.
        #'~' self.start_time = utime.ticks_us()                                     
        ## The "timestamp" for when the task should run next
        #'~'self.next_time = utime.ticks_add(self.start_time, self.interval)    
        
        ## For running in Python only. 
        #  COMMENT OUT if using MicroPython.
        # Sets timestamp for running in Python.
        self.start_time = time.time()
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task.
        '''
        ## UNCOMMENT For Nucleo, gives access to control LED.
        #pinA5=pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        # UNCOMMENT to run with MicroPython
        ## Updates the current timestamp.
        #self.curr_time = utime.ticks_us() 
        ## First check the time and if there is time change execute the code
        #if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
        
        ## For running in Python only.
        #  COMMENT OUT if using MicroPython.
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_LED_ON)
                print(str(self.runs) + ': Initiating :' + str(self.curr_time-self.start_time))
                ## Turn LED ON
                #pinA5.high ()
                
            elif(self.state == self.S1_LED_ON):
                # Run State 1 Code
                
                if(self.runs*self.interval > 10):  # if our tsk runs for 10 seconds, transition
                    self.transitionTo(self.S3_DO_NOTHING)
                else:
                    ## Turn LED ON
                    #pinA5.high ()
                    print(str(self.runs) + ': LED ON :' + str(self.curr_time-self.start_time))
                    self.transitionTo(self.S2_LED_OFF)
                    
            elif(self.state == self.S2_LED_OFF):
                # Run State 2 Code
                ## Turn LED OFF
                #pinA5.low ()
                print(str(self.runs) + ': LED OFF :' + str(self.curr_time-self.start_time))
                self.transitionTo(self.S1_LED_ON)

            elif(self.state == self.S3_DO_NOTHING):
                # Run state 3 code
                print(str(self.runs) + ': Ending :' + str(self.curr_time-self.start_time))
            
            else:
                ## Invalid state code (error handling)
                pass
            
            self.runs += 1  # += adds a number to a variable, changing the variable 
                            # itself in the process (whereas + would not). Similar 
                            # to this, there are the following that also modifies 
                            # the variable: -= , subtracts a value from variable, 
                            # setting the variable to the result. *= , multiplies the 
                            # variable and a value, making the outcome the variable.
            
            ## UNCOMMENT to use with MicroPython
            ## Specifying the next time the task will run
            #self.next_time += utime.ticks_add(self.next_time, self.interval)
            
            ## For running in Python only.
            #  COMMENT OUT if using MicroPython.
            self.next_time = self.next_time + self.interval
            
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run.
        '''
        self.state = newState
        