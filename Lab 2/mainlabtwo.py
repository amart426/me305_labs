#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
@file mainlabtwo.py

This code serves as the main project.

This code is where both the virtual_LED.py and blinkking_LED.py 
program are executed.

@author: Adan Martinez Cruz

@date October 14, 2020
'''

## We import BlinkLED class from blinking_LED.py
from blinking import BlinkLED

## We import VirtualLED class from virtual_LED.py
from virtual_LED import VirtualLED
        

## Creating a task object using the interval. The user can change the time 
#  interval here.
task1 = BlinkLED(2.5)
task2 = VirtualLED(1)

## Run the tasks in sequence over and over again
# Please COMMENT OUT a task if wish to only seen one task at a time
for N in range(200000000): # effectively while(True): 10000000
    # Run task 1
    task1.run()
    # Run task 2
    task2.run()