#!/bin/bash
# -*- coding: utf-8 -*-
"""
@file.py
Brief doc for fibnum.py

Detailed doc for fibnum.py

@author: adan.martinezcruz

@date Sep 24 10:06:00 2020

@package fibnum
Brief doc for the Fibunacci number module

Detailed doc for the encoder module

@author Adan Martinez

@date Sep 24, 2020

"""

# This imports the module for sys and enable the author to make use of its 
# functions and constants.
import sys
## Memoization is used instead of recursion method since we want to efficiently 
# calculate the Fibonacci number for indexies > 100. This can be done by 
# storing data from previous calculations. This eliminates calculating 
# multiple times.
fib_cache = {} # Creates a dictionary.
def fib(idx):
    '''
    @brief     This function defines our Fibonacci number calculator using 
               memoization.
    @details   This method calculates a Fibonacci number corresponding to a 
               specified index. It will first check if the input is cache and 
               if it does only then will it return the value corresponding to
               to the input. Next, it will do some logical test for the input
               value and will return its corresponding Fibonacci value for the
               first two Fibonacci indexies. Lastly, the value is cache 
               and return.
    @param idx An integer specifying the index of the desired 
               Fibonacci number.
    '''
    print ("Calculating Fibonacci number at "
           "index n = {:}.".format(idx))
    ## This first checks if the input exists in the dictionary. If yes, then
    #  it returns the value corresponding to the input.
    if idx in fib_cache:
        return fib_cache[idx]
    # Computes Nth term.
    if idx == 0:
        value = 0
    elif idx == 1:
        value = 1
    else:
        value = fib(idx -1) + fib(idx -2)
    # Cache the value and returns it.
    fib_cache[idx] = value
    return value
    # Return allows the function to return a variable.

if __name__ == "__main__":
    '''
    @brief   This seciton allows the program interfere with user and validates
             that the users input is an integer.
             
    @details This is where the interface with the user takes place. The user
             is ask to enter an integer or enter a string to exit the program.
             The section test the users input. If the input is a digit the
             program converts the input to an integer. Only then, the program 
             prints the correct value for the Fibonacci number enter. 
             If the input cannot be converted to an integer, the program 
             prints out "Error.. input is not a number" and exits the program.
    '''
    ## Input command enables the program to interface with the user. It stores
    #  the users input as user_input.
    user_input = input ("Please enter an integer: \nEnter a string to exit. ")
    # Try command forces python to perfom certain code.
    try:
        ## Converts user_input to an integer and  calls it 'idx'.
        idx = int(user_input)
        print("Input is an integer number. Number = ", idx)
    except ValueError:
        ## IF user_input cannot be converted to an integer it checks for a 
        #  float.
        try:
            # Checks if user_input is a float.
            idx = float(user_input)
            print("Input is a float  number. Number = ", idx)
        except ValueError:
        ## If the user_input is neither an integer nor float, the program
        #  prints out "Error.."
            print("Error.. input is not a number.")
            sys.exit()            
    print(fib(idx))