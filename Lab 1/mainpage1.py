## @file mainpage1.py
#  Brief doc for mainpage1.py
#
#  Detailed doc for mainpage1.py 
#
#  @package mainpage1
#  Brief doc for setting up main page.
#
#  @mainpage
#
#  @author Adan Martinez
#
#  @date Oct 30, 2020
#
#  @section sec_intro Introduction
#  The purpose behind this project is to create a program that calculates the
#  Fibonacci number for indexes from 0 to n>100. 
#
#  @section sec_ref References
#  @ref <https://towardsdatascience.com/memoization-in-python-57c0a738179a>
#
#  @ref <https://www.hashbangcode.com/article/stopping-code-execution-python>
#