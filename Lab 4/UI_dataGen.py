#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file     UI_dataGen.py

@brief    This file implements a finite state machine using encoder.py.

@details  This file calls on the EncoderDriver class and calls the update() 
          method at regular intervals. The objective of this task is to update
          the encoder object at the requisite interval to avoid missing any 
          encoder ticks.
          
Created on Sat Oct 31 00:56:13 2020

@author: Adan Martinez
"""
import shares
import utime
from encoder import EncoderDriver
import array

class Task_ENC:
    '''
    @brief      Create a finite state machine that using the EncoderDriver
                class from the encoder.py file.
                
    @details    This class implements a finite state machine to interface with user 
                designed specifically to run in Micropython.
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT    = 0    
    ## Constant defining State 1
    S1_UPDATE  = 1    
   
    def __init__(self, interval, encoder):
        '''
        @brief            Creates a user_interface object.
        @param interval   Representing the elapsed time count(microsecond) 
                          between user state interface.
        '''
        ## Calls the update() method from EncoderDriver class.
        self.encoder = EncoderDriver.update()
        
        self.getPos = EncoderDriver.get_position()        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*(1e6))
        ## The timestamp for the first iteration 
        self.start_time = utime.ticks_us()
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def DataCollect(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Updating current timestamp
        self.curr_time = utime.ticks_us()    
        
        if(self.state == self.S0_INIT):
            ## Prints encoder position
            ## Transition to State 1.
            self.transitionTo(self.S1_UPDATE)
                
        elif(self.state == self.S1_UPDATE):
           
            ## Run update() from EncoderDriver and stores the value as val.
            self.val = self.encoder
            ## Adds element to the end of the time array.
            shares.time.append(self.next_time)
            ## Adds elements to the end of the array.
            shares.data.append(self.val)
            ## Transition to State 1.
            self.transitionTo(self.S2_UPDATE)
            
        elif(self.state == self.S2_UPDATE):
            ## Run update() from EncoderDriver and stores the value as val.
            self.val = self.encoder
            ## Adds element to the end of the time array.
            shares.time.append(self.next_time)
            ## Adds elements to the end of the array
            shares.data.append(self.val)
            ## Transition to State 1.
            self.transitionTo(self.S1_UPDATE)
            
        # Specifying the next time the task will run
        self.next_time += utime.ticks_add(self.next_time, self.interval)
        
    def transitionTo(self, newState):
        
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    def get_data(self):
        
        '''
        @brief      Returns data
        '''
        return shares.data
        