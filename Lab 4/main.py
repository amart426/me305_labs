#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file     main.py

@brief    This file concerns hardware only.

@details  This file main goal is to take comand from user and collect data 
          from certain intervals of time. This code it only meant to run in 
          MicroPython
          
Created on Sat Oct 31 00:56:13 2020

@author: Adan Martinez

Created on Tue Oct 27 23:41:10 2020

@author: adanmartinez
"""
import shares
from UI_dataGen import DataCollect
from pyb import UART


myuart = UART(2)

print('Please enter "G" to start collecting data: ')

while True:
    ## Only runs when evers there is an input.
    if myuart.any() != 0:
        
        val = myuart.readchar()
        myuart.write('You sent an ASCII ' + str(val) + ' to the Nucleo')
        timer = range(100)
        encodervals = range(100)
        if val == 71: # G
            for n in range(len(timer)):
                DataCollect()
                
                print('{:}, {:}'. format(timer[n], encodervals[n]))
                
        elif val == 83: # S
            DataCollect()
            print('Terminating data collection!')
        else:
            pass
    else:
        print('Error')
        pass
        
        