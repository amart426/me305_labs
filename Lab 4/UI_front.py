#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file     UI_dataGen.py

@brief    This file serves as the User Interface front-end.

@details  This file send users input to UI_dataGen. Whenever the user stops
          data collection the data is saved in an array. The data is then 
          saved and plotted versus time.
          
Created on Tue Oct 27 23:42:53 2020

@author: adanmartinez
"""
import shares
import matplotlib.pyplot as plt
import numpy as np
import serial

## Connects to port. Change port name if different. THe baudrate must match
#  that of putty/iTerm.
ser = serial.Serial(port='COM3',baudrate=115200,timeout=1)

def sendChar():
    inv = input('Press G to start collecting data, press S anytime to stop collecting data.') 
    ser.write(str(inv).encode('ascii')) 
    myval = ser.readline().decode('ascii') 
    return myval


for n in range(1):
    sendChar()
    ## Create a plot.
    plt.plot(shares.time, shares.data, 'r')
    plt.xlabel('Time [s]')
    plt.ylabe('Angle [deg]')
    plt.grid(True)
    plt.show()
    
    ## Save data to a .csv file.
    np.savetxt('Lab4.csv', shares.data, delimiters=',')
    
    print(sendChar())
    
ser.close()